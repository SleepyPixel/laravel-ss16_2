<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Character extends Model
{
    use SoftDeletes;

    const ATTRIBUTES_TOTAL_DEFAULT = 5 + 5 + 5 + 3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'character';

    protected $fillable = ['name', 'status', 'race', 'age', 'gender', 'skin_color', 'eye_color', 'hair_style', 'hair_color', 'strength', 'intelligence', 'agility'];

}
