<?php

namespace App\Broadcasting;

use App\Http\Controllers\WebSocketController;
use App\Service\WebsocketMessage;
use App\User;

abstract class AbstractChannel
{
    protected $connections;
    protected $listener_id_list = [];

    const SYSTEM_CHANNEL = 'system';
    const CHAT_CHANNEL = 'chat';

    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct(&$connections)
    {
        $this->connections = &$connections;
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param $connection_resource_id
     * @return bool
     */
    public function join($connection_resource_id)
    {
        //
        $this->listener_id_list[$connection_resource_id] = 1;
        return true;
    }
    public function leave($connection_resource_id)
    {
        unset($this->listener_id_list[$connection_resource_id]);
    }

    public function sendMessage($listener, ?array $message)
    {
        if (!empty($message)) {
            $message = json_encode($message);
            echo "Send message to $listener:" . $message . "\n";
            $this->connections[$listener][WebSocketController::CONNECTION_KEY]->send($message);
        }
    }

    public function sendMessageToAll(?array $message)
    {
        //echo "Send message to All:" . $message . "\n";
        $channel_listeners = $this->listener_id_list;

        foreach ($channel_listeners as $channel_listener => $channel_listener_data) {
            $this->sendMessage(
                $channel_listener,
                $message
//                [
//                    'content' => $message['content'],
////                    'from_user_id' => $fromUserId,
////                    'from_resource_id' => $conn->resourceId
//                ]
            );
        }
    }
}
