<?php

namespace App\Http\Controllers;

use App\Character;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class CharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var User $user */
        $user = request()->user();
        $characters = $user->characters()->get();
//        var_dump(empty($user->characters()->get()));
        return view('user/character', [
            "characters" => $characters,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        do {
            if (!$request->validate([
                'name' => 'required|max:255',
            ])) {
                break;
            }

            $character = new Character();
            $character->fill($request->all());
            $character->user_id = $request->user()->id;
            $character->attributes_total = Character::ATTRIBUTES_TOTAL_DEFAULT;

            if (($result = $character->save())) {
                $request->session()->flash('success', array_merge((array) $request->session()->get('success'), ['character' => 'Character create successful!']));
            } else {
                $request->session()->flash('errors', array_merge((array) $request->session()->get('errors'), ['character' => 'Character create failed!']));
            }
        }
        while (false);

        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        do {
            if (!$request->validate([
                'name' => 'required|max:255',
            ])) {
                break;
            }

            /** @var Character $character */
            $character = Character::find($id);

            if (!$character->id) {
                $request->session()->flash('errors', array_merge((array) $request->session()->get('errors'), ['character' => 'Character not found!']));
                break;
            }
            if ($character->user_id != $request->user()->id) {
                $request->session()->flash('errors', array_merge((array) $request->session()->get('errors'), ['character' => 'Character own to another user! ' . $character->user_id . '-' . $request->user()->id]));
                break;
            }

            $character->fill($request->all());

            if (($result = $character->save())) {
                $request->session()->flash('success', array_merge((array) $request->session()->get('success'), ['character' => 'Character update successful!']));
            } else {
                $request->session()->flash('errors', array_merge((array) $request->session()->get('errors'), ['character' => 'Character update failed!']));
            }
        }
        while (false);

        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        do {
            $request = request();
            $character = Character::find($id);

            if (($result = $character->delete())) {
                $request->session()->flash('success', array_merge((array) $request->session()->get('success'), ['character' => 'Character delete successful!']));
            } else {
                $request->session()->flash('errors', array_merge((array) $request->session()->get('errors'), ['character' => 'Character delete failed!']));
            }
        }
        while (false);

        return back()->withInput();
    }
}
