<?php

namespace App\Http\Controllers;

use App\Broadcasting\AbstractChannel;
use App\Broadcasting\ChatChannel;
use App\Broadcasting\SystemChannel;
use App\Service\WebsocketMessage;
use App\Service\WebsocketPlayer;
use Illuminate\Http\Request;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class WebSocketController extends Controller implements MessageComponentInterface
{
    private $connections = [];
    /** @var WebSocketPlayer $websocket_player */
    private $websocket_player;
    /** @var AbstractChannel[] $channels  */
    private $channels = [];

    const CONNECTION_KEY = 'connection';

    public function __construct()
    {
        $this->websocket_player = new WebsocketPlayer();

        $this->channels['system_channel'] = new SystemChannel($this->connections);
        $this->channels['chat_channel'] = new ChatChannel($this->connections);
    }

    /**An error has occurred with user
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    function onOpen(ConnectionInterface $conn)
    {
        $connect_id = $conn->resourceId;
        $player_name = 'Player' . $connect_id;

        $this->connections[$connect_id] = [
            WebSocketController::CONNECTION_KEY => $conn,
            'user_id' => null,
        ];
        echo "New connection. ResourceId: " . $connect_id . "\n";

        // Добавляем клиента в соответствующие каналы
        $this->channels['system_channel']->join($connect_id);
        $this->channels['chat_channel']->join($connect_id);

        // Отправляем мир новому клиенту


        // Отправляем всех игроков новому клиенту
        /*$players = $this->websocket_player->getAllPlayer();

        foreach ($players as $player) {
            $this->channels['system_channel']->sendMessage($connect_id, [
                'type' => WebsocketMessage::TYPE_SYSTEM,
                'action' => WebsocketMessage::ACTION_PLAYER_CREATE,
                'options' => [
                    'connect_id' => $player['connect_id'],
                    'name' => $player['name'],
                    'position_x' => $player['position_x'],
                    'position_z' => $player['position_z'],
                ],
            ]);
        }*/

        // Отправляем нового клиента всем клиентам
        $this->channels['system_channel']->sendMessageToAll([
            'type' => WebsocketMessage::TYPE_SYSTEM,
            'action' => WebsocketMessage::ACTION_PLAYER_CREATE,
            'options' => [
                'connect_id' => $connect_id,
                'name' => 'Player' . $player_name,
                'position_x' => 3,//$this->message_decoded[WebsocketMessage::OPTIONS_KEY]['position_x'],
                'position_z' => 3,//$this->message_decoded[WebsocketMessage::OPTIONS_KEY]['position_z'],
            ],
        ]);

        // Отправляем новому клиенту информацию о его соединении
        /*$this->channels['system_channel']->sendMessage($connect_id, [
            'type' => WebsocketMessage::TYPE_SYSTEM,
            'action' => WebsocketMessage::ACTION_CONNECT_INIT,
            'options' => [
                'connect_id' => $connect_id,
            ],
        ]);*/

        // Запоминаем нового игрока в сервисе
        $this->websocket_player->addPlayer([
            'connect_id' => $connect_id,
            'name' => $player_name,
            'position_x' => 3,//$this->message_decoded[WebsocketMessage::OPTIONS_KEY]['position_x'],
            'position_z' => 3,//$this->message_decoded[WebsocketMessage::OPTIONS_KEY]['position_z'],
        ]);
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    function onClose(ConnectionInterface $conn)
    {
        echo "Disconnect. ResourceId: " . $conn->resourceId . "\n";
        $disconnectedId = $conn->resourceId;
        unset($this->connections[$disconnectedId]);

        // Покидаем каналы
        $this->channels['system_channel']->leave($disconnectedId);
        $this->channels['chat_channel']->leave($disconnectedId);

        foreach($this->connections as &$connection)
            $connection['conn']->send([
                'offline_user' => $disconnectedId,
                'from_user_id' => 'server control',
                'from_resource_id' => null
            ]);
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    function onError(ConnectionInterface $conn, \Exception $e)
    {
        $userId = $this->connections[$conn->resourceId]['user_id'];
        echo "An error has occurred with user $userId [{$conn->resourceId}]:\n{$e->getFile()}::{$e->getLine()} {$e->getMessage()}\n";
        unset($this->connections[$conn->resourceId]);
        $conn->close();
    }

    /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $conn The socket/connection that sent the message to your application
     * @param  string $message The message received.
     * Example:
     * [
     *      "type": "init", // появление персонажа
     * ]
     * [
     * "type": "system",
     * "action": "object_change", //"type": "init"
     * "target": 123,
     * "options": [
     *      "action": "move",
     *      "position_x": "1",
     *      "position_y": "1",
     * ]
     * ]
     * [
     *      "type": "vision", // наблюдение действий
     * ]
     * @throws \Exception
     */
    function onMessage(ConnectionInterface $conn, $message)
    {
        $connect_id = $conn->resourceId;

        $websocket_message = new WebsocketMessage($message, $connect_id, $this->websocket_player);

        // Разбираем сообщение, получаем канал и передаем его туда
        if ($websocket_message->isValid()) {
            echo "[{$connect_id}] Good message: {$message} \n";

            if (
                $websocket_message->getType() == WebsocketMessage::TYPE_SYSTEM
                && $websocket_message->getAction() == WebsocketMessage::ACTION_GET_PLAYERS
            ) {
                // todo это действие отправляется не стандартно, стоит переделать на одно сообщение?
                // Примечание. Здесь на клиента отправляются все игроки, включая его самого.
                // Но он сам уже был отправлен себе при создании соединения. Клиент это отфильтрует, но есть над чем подумать.
                $players = $this->websocket_player->getAllPlayer();

                foreach ($players as $player) {
                    $this->channels['system_channel']->sendMessage($connect_id, [
                        'type' => WebsocketMessage::TYPE_SYSTEM,
                        'action' => WebsocketMessage::ACTION_PLAYER_CREATE,
                        'options' => [
                            'connect_id' => $player['connect_id'],
                            'name' => $player['name'],
                            'position_x' => $player['position_x'],
                            'position_z' => $player['position_z'],
                        ],
                    ]);
                }

                // Отправляем новому клиенту информацию о собственном игроке и соединении
                $this->channels['system_channel']->sendMessage($connect_id, [
                    'type' => WebsocketMessage::TYPE_SYSTEM,
                    'action' => WebsocketMessage::ACTION_CONNECT_INIT,
                    'options' => [
                        'connect_id' => $connect_id,
                    ],
                ]);
            }
            elseif ($websocket_message->getType() == WebsocketMessage::TYPE_SYSTEM) {
                $this->channels['system_channel']->sendMessageToAll($websocket_message->getResultToAll());
                $this->channels['system_channel']->sendMessage($connect_id, $websocket_message->getResultToSender());
            }
            if ($websocket_message->getType() == WebsocketMessage::TYPE_SOUND) {
                $this->channels['chat_channel']->sendMessageToAll($websocket_message->getResultToAll());
            }
        }
        else {
            echo "[{$connect_id}] Bad message: {$message} \n";
            echo "ERROR: " . $websocket_message->getError();

            foreach ($this->connections as $resourceId => &$connection) {
                $connection[WebSocketController::CONNECTION_KEY]->send(json_encode([$connect_id => $message]));
                if($connect_id != $resourceId)
                    $onlineUsers[$resourceId] = $connection['user_id'];
            }
        }
    }
}
