<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CharacterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /** @var User $user */
        $user = $request->user();
        var_dump(empty($user->characters()->count()));
        var_dump($user->characters()->get()->count());
        var_dump($request->user());

        if (empty($user->characters()->count())) {
            return redirect()->route('character.index');
        }
        return $next($request);
    }
}
