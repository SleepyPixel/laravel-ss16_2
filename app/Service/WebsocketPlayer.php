<?php

namespace App\Service;

/**
 * Class WebSocketPlayer
 * Класс хранит и обрабатывает игроков сервера
 * @package App\Service
 */
class WebsocketPlayer
{
    protected $players = [];

    public function addPlayer($player)
    {
        $this->players[$player['connect_id']] = $player;
    }

    public function changePlayer(int $connect_id, array $player)
    {
        if (!isset($this->players[$connect_id])) {
            WebsocketLogger::processError("Try change player which not exist: $connect_id");
            return false;
        }

        $this->players[$connect_id] = array_replace($this->players[$connect_id], $player);

        if (!isset($this->players[$connect_id])) {
            WebsocketLogger::processError("Error while change player[{$connect_id}]:" . $this->players[$connect_id] . "|");
            return false;
        }

        return true;
    }

    public function getAllPlayer()
    {
        return $this->players;
    }
}
