<?php

namespace App\Service;

/**
 * Class WebsocketMessage
 * Класс является "пониманием" структуры сообщений и занимается их обработкой
 * @package App\Service
 */
class WebsocketMessage
{
    // Структура
    protected $type; //*
    protected $action; //*
    protected $options;

    // Базовые инициации
    /** @var WebSocketPlayer $web_socket_player */
    protected $websocket_player;
    protected $connect_id;
    protected $message_source = null;

    // Константы
    const OPTIONS_KEY = 'options';

    const TYPE_SYSTEM = 'system'; // все передвижения персонажа, все его взаимодействия с окружением
    const TYPE_SOUND = 'sound'; // звуковое событие
    const TYPE_TEXT_MESSAGE = 'text_message';
    const TYPE_VISION = 'vision';

    const ACTION_CONNECT_INIT = 'init';
    const ACTION_GET_MAP = 'get_map';
    const ACTION_GET_PLAYERS = 'get_players';
    const ACTION_PLAYER_CREATE = 'player_create';
    const ACTION_PLAYER_CHANGE = 'player_change';
    const ACTION_OBJECT_CREATE = 'object_create';
    const ACTION_OBJECT_OPERATE = 'object_operate';

    const ACTION_SPEAK = 'speak';

    // Рассчитываемые свойства
    protected $message_decoded = [];
    protected $is_valid = false;
    protected $error = '';

    protected $result_to_sender;
    protected $result_to_all;

    protected $type_allowed_action = [
        WebsocketMessage::TYPE_SYSTEM => [
            WebsocketMessage::ACTION_CONNECT_INIT,
            WebsocketMessage::ACTION_GET_MAP,
            WebsocketMessage::ACTION_GET_PLAYERS,
            WebsocketMessage::ACTION_PLAYER_CREATE,
            WebsocketMessage::ACTION_PLAYER_CHANGE,
            WebsocketMessage::ACTION_OBJECT_OPERATE,
        ],
        WebsocketMessage::TYPE_SOUND => [
            WebsocketMessage::ACTION_SPEAK,
        ],
    ];

    public function __construct(string $message, int $connect_id, WebSocketPlayer &$websocket_player)
    {
        $this->websocket_player = &$websocket_player;
        $this->connect_id = $connect_id;
        $this->message_source = $message;
        $this->message_decoded = json_decode($message, true);

        if (empty($this->message_decoded)) {
            $this->is_valid = false;
            $this->error = "[" . __CLASS__ . "] [" . __LINE__ . "] Cant decode message: {$message}.\n";
            return;
        }

        // Проверяем действие в сообщении
        // @todo а надо ли?
        if (
            !$this->getAction() ||
            !isset($this->type_allowed_action[$this->getType()]) ||
            !in_array($this->getAction(), $this->type_allowed_action[$this->getType()])
        ) {
            $this->is_valid = false;
            $this->error = "[" . __CLASS__ . "] [" . __LINE__ . "] Wrong action ({$this->getType()}) ({$this->getAction()}) (".serialize($this->type_allowed_action[$this->getType()]??null).") or not allowed.\n";
            return;
        }

        $this->is_valid = true;

        $this->doAction();
    }

    public function isValid()
    {
        return $this->is_valid;
    }
    public function getError()
    {
        return $this->error;
    }

    public function getType()
    {
        return $this->message_decoded['type'] ?? null;
    }

    public function getAction()
    {
        return $this->message_decoded['action'] ?? null;
    }

    public function getKey()
    {
        return $this->message_decoded['key'] ?? null;
    }

    public function getMessageDecoded()
    {
        return $this->message_decoded;
    }

    public function getMessageSource()
    {
        return $this->message_source;
    }

    protected function doAction()
    {
        if ($this->getAction() == WebsocketMessage::ACTION_CONNECT_INIT) { // совместить с отправкой в Connect, сейчас не работает
            $player = [
                'connect_id' => $this->connect_id,
                'name' => $this->message_decoded[WebsocketMessage::OPTIONS_KEY]['name'],
                'position_x' => 2,//$this->message_decoded[WebsocketMessage::OPTIONS_KEY]['position_x'],
                'position_z' => 2,//$this->message_decoded[WebsocketMessage::OPTIONS_KEY]['position_z'],
            ];
            $this->websocket_player->addPlayer($player);

//            $this->setResultToSender([
//                'action' => 'init_self',
//                'player_id' => $this->connect_id,
//            ]);
            $this->setResultToAll([
                'action' => 'create',
                'key' => $this->getKey(),
                WebsocketMessage::OPTIONS_KEY => [
                    'type' => 'player',
                    'connect_id' => $this->connect_id,
                    'position_x' => 20,
                    'position_z' => 20,
                ],
            ]);
        }
        if ($this->getAction() == WebsocketMessage::ACTION_GET_MAP) {
            $this->setResultToSender([
                'type' => $this->getType(),
                'action' => $this->getAction(),
                'key' => $this->getKey(),
                WebsocketMessage::OPTIONS_KEY => [
                    'map' => json_encode(MapService::getMapJson()),
                ],
            ]);
        }
        if ($this->getAction() == WebsocketMessage::ACTION_OBJECT_OPERATE) {
            $object_id = $this->message_decoded[WebsocketMessage::OPTIONS_KEY]['object_id'];
            $action = $this->message_decoded[WebsocketMessage::OPTIONS_KEY]['action'];

            WebsocketObject::addObjectData($object_id, ["action" => $action]);

            $this->setResultToAll([
                'type' => $this->getType(),
                'action' => $this->getAction(),
                'key' => $this->getKey(),
                WebsocketMessage::OPTIONS_KEY => [
                    'connect_id' => $this->connect_id,
                    'object_id' => $object_id,
                    'action' => $action,
                ],
            ]);
        }
        if ($this->getAction() == WebsocketMessage::ACTION_PLAYER_CHANGE) {
            $position_x = $this->getMessageDecoded()[WebsocketMessage::OPTIONS_KEY]["position_x"];
            $position_z = $this->getMessageDecoded()[WebsocketMessage::OPTIONS_KEY]["position_z"];

            $this->websocket_player->changePlayer(
                $this->connect_id,
                [
                    'position_x' => $position_x,
                    'position_z' => $position_z,
                ]
            );

            $this->setResultToAll([
                'type' => $this->getType(),
                'action' => $this->getAction(),
                'key' => $this->getKey(),
                WebsocketMessage::OPTIONS_KEY => [
                    'type' => 'move',
                    'connect_id' => $this->connect_id,
                    'position_x' => $position_x,
                    'position_z' => $position_z,
                ],
            ]);
        }
        if ($this->getAction() == WebsocketMessage::ACTION_SPEAK) {
            $this->setResultToAll([
                'type' => $this->getType(),
                'action' => $this->getAction(),
                'key' => $this->getKey(),
                WebsocketMessage::OPTIONS_KEY => [
                    'type' => 'speak',
                    'connect_id' => $this->connect_id,
                    'name' => 'SomePlayer',
                    'text' => $this->getMessageDecoded()[WebsocketMessage::OPTIONS_KEY]["text"],
                ],
            ]);
        }
    }

    protected function setResultToSender($message)
    {
        $this->result_to_sender = $message;
    }

    public function getResultToSender()
    {
        return $this->result_to_sender;
    }

    protected function setResultToAll($message)
    {
        $this->result_to_all = $message;
    }

    public function getResultToAll()
    {
        return $this->result_to_all;
    }
}
