<?php

namespace App\Service;

use Illuminate\Support\Facades\Storage;

/**
 * Class MapService
 * Класс работает с картой игры
 * @package App\Service
 */
class MapService
{
    protected static $map = null;
    private static $object_id = 1;

    /**
     * @return array
     */
    public static function getMapJson() : array
    {
        self::generateMap();
        $map = self::$map;

        foreach ($map as &$map_i) {
            foreach ($map_i as &$map_j) {
                foreach ($map_j as $map_cell_key => &$map_cell) { // $map_cell_key = floor/top/bottom/left/right/items...
                    if ($map_cell_key == "items") {
                        foreach ($map_cell as $item_key => $item) {
                            if (isset($map_cell["object_id"]) && ($data = WebsocketObject::getObjectData($item["object_id"]))) {
                                $map_cell[$item_key]["data"] = array_merge($map_cell[$item_key]["data"], $data);
                            }
                        }
                    }
                    if ($map_cell && isset($map_cell["object_id"]) && ($data = WebsocketObject::getObjectData($map_cell["object_id"]))) {
                        $map_cell["data"] = $data;
                    }
                }
            }
        }

        return $map;
        //$map_decoded = self::$map_decoded;

        /*try {
            $object_id = 0;
            $map_decoded = json_decode(Storage::disk('local')->get('maps/mapv1.json'), true) ?? [];

            foreach ($map_decoded as &$line) {
                foreach ($line as &$cell) {
                    foreach ($cell as &$cell_object) {
                        if (!empty($cell_object)) {
                            $cell_object = [
                                "object_id" => $cell_object . "_" . $object_id,
                                "type" => $cell_object,
                            ];
                            $object_id++;
                        }
                    }
                }
            }
            unset($cell);
            unset($cell_object);
        }
        catch (\Exception $exception) {
            WebsocketLogger::processException($exception);
        }

        return $map_decoded;*/
    }

    public static function generateMap()
    {
        self::$map = [];
        self::$object_id = 1;

        //space field
        $i_min = 0; $i_max = $i_min + 99;
        $j_min = 0; $j_max = $j_min + 99;
        for ($i = $i_min; $i <= $i_max; $i++) {
            for ($j = $j_min; $j <= $j_max; $j++) {
                self::$map[$i][$j] = [
                    "floor" => [
                        "object_id" => "space_" . self::$object_id++,
                        "type" => "space",
                    ],
                ];
            }
        }


        /*for ($i = 0; $i <= 10; $i++) {
            for ($j = 0; $j <= 10; $j++) {
                $map[$i][$j] = [
                    "floor" => [
                        "object_id" => "floor_" . self::$object_id++,
                        "type" => "floor",
                    ],
                    "top" => null,
                    "bottom" => null,
                    "right" => null,
                    "left" => null,
                ];

                if (5 === $i) {
                    $map[$i][$j]["right"] = [
                        "object_id" => "floor_" . self::$object_id++,
                        "type" => "wall",
                    ];

                    if (3 === $j) {
                        $map[$i][$j]["right"] = [
                            "object_id" => "door_" . self::$object_id++,
                            "type" => "door",
                        ];
                        $map[$i][$j]["top"] = [
                            "object_id" => "door_" . self::$object_id++,
                            "type" => "door",
                        ];
                    }
                }
                if (5 === $j) {
                    $map[$i][$j]["top"] = [
                        "object_id" => "floor_" . self::$object_id++,
                        "type" => "wall",
                    ];

                    if (3 === $i) {
                        $map[$i][$j]["top"] = [
                            "object_id" => "door_" . self::$object_id++,
                            "type" => "door",
                        ];
                    }
                }
                if (10 === $j) {
                    $map[$i][$j]["top"] = [
                        "object_id" => "floor_" . self::$object_id++,
                        "type" => "wall",
                    ];

                    if (3 === $i) {
                        $map[$i][$j]["top"] = [
                            "object_id" => "door_" . self::$object_id++,
                            "type" => "door",
                        ];
                    }
                }
            }
        }*/

        //room1
        $i_min = 0; $i_max = $i_min + 6;
        $j_min = 0; $j_max = $j_min + 6;
        $doors = [
            ["right",6,2],
        ];
        $items = [
            [
                "title" => "LEDLight",
                "name" => "Lights/LampLEDLong",
                "i" => 1,
                "j" => 3,
                "position" => [-1.1, 2.3, 0],
            ],
        ];
        self::buildRoom($i_min, $i_max, $j_min, $j_max, $doors, $items, [], []);

        //hall1
        $i_min = 6; $i_max = $i_min + 4;
        $j_min = 0; $j_max = $j_min + 30;
        $no_wall = [
            "10_18" => 1,
            "10_19" => 1,
            "10_20" => 1,
            "10_21" => 1,
            "10_22" => 1,

//            "6_0" => 1,
            "6_1" => 1,
            "6_2" => 1,
            "6_3" => 1,
            "6_4" => 1,
        ];
        self::buildRoom($i_min, $i_max, $j_min, $j_max, [], [], $no_wall);

        //hall2
        $i_min = 6; $i_max = $i_min + 30;
        $j_min = 18; $j_max = $j_min + 4;
        $no_wall = [
            "6_18" => 1,
            "7_18" => 1,
            "8_18" => 1,
            "9_18" => 1,
            "10_18" => 1,

            "13_18" => 1,

            "6_22" => 1,
            "7_22" => 1,
            "8_22" => 1,
            "9_22" => 1,
            "10_22" => 1,
        ];
        self::buildRoom($i_min, $i_max, $j_min, $j_max, [], [], $no_wall);

        //bar
        $i_min = 11; $i_max = $i_min + 7; // vert
        $j_min = 11; $j_max = $j_min + 6; // hor
        $doors = [
            ["top",13,17],
        ];
        $no_wall = [
            "18_11" => 1,
            "18_12" => 1,
            "18_13" => 1,
            "18_14" => 1,
            "18_15" => 1,
            "18_16" => 1,
            "18_17" => 1,

            "11_11" => 1,
            "11_12" => 1,
            "11_13" => 1,
            "11_14" => 1,
            "11_15" => 1,
            "11_16" => 1,
            "11_17" => 1,
        ];
        $items = [
            [
                "name" => "BarTable",
                "i" => 15,
                "j" => 15,
                "position" => [-0.8, 0, 0]
            ],
            [
                "name" => "BarTable",
                "i" => 15,
                "j" => 14,
                "position" => [-0.8, 0, 0]
            ],
            [
                "title" => "BarTable",
                "name" => "BarTable2",
                "i" => 17,
                "j" => 15,
                "position" => [-0.55, 0, 0]
            ],
            [
                "title" => "BarTable",
                "name" => "BarTable2",
                "i" => 17,
                "j" => 14,
                "position" => [-0.55, 0, 0]
            ],
            [
                "title" => "Booze-O-Mat",
                "name" => "Booze-O-Mat",
                "i" => 17,
                "j" => 14,
                "position" => [0.3, 1.18, 0],
                "rotation" => [-90, 90, 0]
            ],
            [
                "name" => "Glass",
                "i" => 15,
                "j" => 15,
                "position" => [0.3, 0.85, 0],
                "rotation" => [0, 0, 0]
            ],
            [
                "title" => "LEDLight",
                "name" => "Lights/LampLEDLong",
                "i" => 12,
                "j" => 14,
                "position" => [-1.1, 2.3, 0],
                "rotation" => [0, 0, 0]
            ],
            [
                "title" => "LEDLight",
                "name" => "Lights/LampLEDLong",
                "i" => 17,
                "j" => 14,
                "position" => [1.1, 2.3, 0],
                "rotation" => [0, 180, 0]
            ],
        ];
        self::buildRoom($i_min, $i_max, $j_min, $j_max, $doors, $items);

        //return self::$map;
    }

    private static function buildRoom($i_min, $i_max, $j_min, $j_max, $doors = [], $items = [], $no_wall = [], $lights = [])
    {
        for ($i = $i_min; $i <= $i_max; $i++) {
            for ($j = $j_min; $j <= $j_max; $j++) {
                self::$map[$i][$j] = [
                    "floor" => [
                        "object_id" => "floor_" . self::$object_id++,
                        "type" => "floor",
                    ],
                    "top" => null,
                    "bottom" => null,
                    "right" => null,
                    "left" => null,
                ];

                if (!isset($no_wall[$i . "_" . $j])) {
                    if ($i_min === $i) {
                        self::$map[$i][$j]["left"] = [
                            "object_id" => "wall_" . self::$object_id++,
                            "type" => "wall",
                        ];
                    }
                    if ($j_min === $j) {
                        self::$map[$i][$j]["bottom"] = [
                            "object_id" => "wall_" . self::$object_id++,
                            "type" => "wall",
                        ];
                    }
                    if ($i_max === $i) {
                        self::$map[$i][$j]["right"] = [
                            "object_id" => "wall_" . self::$object_id++,
                            "type" => "wall",
                        ];
                    }
                    if ($j_max === $j) {
                        self::$map[$i][$j]["top"] = [
                            "object_id" => "wall_" . self::$object_id++,
                            "type" => "wall",
                        ];
                    }
                }
            }
        }

        if ($doors)
            foreach ($doors as $door) {
                self::$map[$door[1]][$door[2]][$door[0]] = [
                    "object_id" => "door_" . self::$object_id++,
                    "type" => "door",
                ];
            }

        if ($items)
            foreach ($items as $item) {
                self::$map[$item["i"]][$item["j"]]['items'] = self::$map[$item["i"]][$item["j"]]['items'] ?? [];
                self::$map[$item["i"]][$item["j"]]['items'][] = [
                    "object_id" => "item_" . self::$object_id++,
                    "title" => $item["title"] ?? $item["name"],
                    "name" => $item["name"],
                    "data" => [
                        "position" => $item["position"] ?? null,
                        "rotation" => $item["rotation"] ?? null,
                    ]
                ];
            }

        if ($lights)
            foreach ($lights as $light) {
                self::$map[$light["i"]][$light["j"]]['lights'] = self::$map[$light["i"]][$light["j"]]['lights'] ?? [];
                self::$map[$light["i"]][$light["j"]]['lights'][] = [
                    "object_id" => "light_" . self::$object_id++,
                    "name" => $light["name"],
                ];
            }
    }
}
