<?php

namespace App\Service;

/**
 * Class WebsocketLogger
 * Класс отвечает за обработку сообщений и ошибок, которые так или иначе надо сохранить или отобразить
 * @package App\Service
 */
class WebsocketLogger
{

    public static function processError(string $message)
    {
        echo "ERROR: " . $message ."\n";
    }

    public static function processException(\Exception $exception)
    {
        echo "EXCEPTION: [" . $exception->getFile() . ":" . $exception->getLine() . "]" . $exception->getMessage() ."\n";
    }
}
