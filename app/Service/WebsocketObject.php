<?php

namespace App\Service;

/**
 * Class WebsocketObject
 * Класс хранит и обрабатывает игроков сервера
 * @package App\Service
 */
class WebsocketObject
{
    protected static $objects = [];

    public static function addObjectData($object_id, $data)
    {
        self::$objects[$object_id] = $data;
    }

    public static function getObjectData(string $object_id)
    {
        if (!isset(self::$objects[$object_id])) {
            return false;
        }

        return self::$objects[$object_id];
    }
}
