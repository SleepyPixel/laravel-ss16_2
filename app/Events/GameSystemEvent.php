<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GameSystemEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('system.' . 1);
//        return new PrivateChannel('chat.'.$this->comment->task_id);
    }

    /**
     * По умолчанию Laravel сериализует все публичные (public) свойства класса и передает их клиенту. Это можно изменить, вернув необходимую информацию этим методом.
     * @return array
     * @throws \Throwable
     */
    /*public function broadcastWith()
    {
        return [
            'view' => view('tasks.comments.single', ['i' => $this->comment])->render()
        ];
    }*/
}
