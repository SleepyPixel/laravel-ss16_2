<?php
/** @var array $characters */
?>
@extends('layouts.app')

@section('content')
<div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Characters</h1>
</div>

<div class="container">
    <div class="card-deck mb-2 text-center">
        <?php foreach ($characters as $character) : ?>
        <div class="card mb-4 box-shadow" style="width: 48%; flex: none; margin: 0 1%;">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal">{{ $character->name }}</h4>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route("character.update", ["id" => $character->id]) }}">
                    <div class="<?php /*form-row align-items-center*/ ?>">
                        @csrf
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="character_create_name">Name</span>
                            </div>
                            <input type="text" name="name" class="form-control" id="character_create_name" value="{{ $character->name }}">

                            <input type="checkbox" checked data-toggle="toggle" data-onstyle="outline-success" data-offstyle="outline-danger" value="1" name="status" id="character_create_status">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ml-3">
                                <label class="input-group-text" for="character_create_race">Race</label>
                            </div>
                            <select class="custom-select" id="character_create_race" name="race">
                                <option value="human">human</option>
                            </select>

                            <div class="input-group-prepend ml-3">
                                <span class="input-group-text" id="character_create_age">Age</span>
                            </div>
                            <input type="text" name="age" class="form-control" id="character_create_age" value="20">

                            <div class="input-group-prepend ml-3">
                                <label class="input-group-text" for="character_create_gender">Gender</label>
                            </div>
                            <select class="custom-select" id="character_create_gender" name="gender">
                                <option value="male">male</option>
                                <option value="female">other</option>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ml-3">
                                <label class="input-group-text" for="character_create_skin_color">Skin color</label>
                            </div>
                            <select class="custom-select" id="character_create_skin_color" name="skin_color">
                                <option value="white">white</option>
                                <option value="black">black</option>
                            </select>

                            <div class="input-group-prepend ml-3">
                                <label class="input-group-text" for="character_create_eye_color">Eye color</label>
                            </div>
                            <select class="custom-select" id="character_create_eye_color" name="eye_color">
                                <option value="brown">brown</option>
                                <option value="grey">grey</option>
                                <option value="green">green</option>
                                <option value="blue">blue</option>
                                <option value="red">red</option>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ml-3">
                                <label class="input-group-text" for="character_create_hair_style">Hair style</label>
                            </div>
                            <select class="custom-select" id="character_create_hair_style" name="hair_style">
                                <option value="bold">bold</option>
                                <option value="short">short</option>
                            </select>

                            <div class="input-group-prepend ml-3">
                                <label class="input-group-text" for="character_create_hair_color">Hair color</label>
                            </div>
                            <select class="custom-select" id="character_create_hair_color" name="hair_color">
                                <option value="black">black</option>
                                <option value="white">white</option>
                                <option value="brown">brown</option>
                                <option value="grey">grey</option>
                                <option value="green">green</option>
                                <option value="blue">blue</option>
                                <option value="red">red</option>
                            </select>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ml-3">
                                <span class="input-group-text" id="character_create_free">Attribute Points</span>
                            </div>
                            <input type="text" name="_attribute_points" disabled class="form-control" id="character_create_free" value="3">
                        </div>
                        <div>
                            <label for="character_create_strength">Strength</label>
                            <input id="character_create_strength" name="strength" type="text"
                                   data-provide="slider"
                                   data-slider-ticks="[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"
                                   data-slider-ticks-labels='["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]'
                                   data-slider-min="3"
                                   data-slider-max="6"
                                   data-slider-step="1"
                                   data-slider-value="5"
                                   data-slider-tooltip="hide" />
                        </div>
                        <div>
                            <label for="character_create_intelligence">Intelligence</label>
                            <input id="character_create_intelligence" name="intelligence" type="text"
                                   data-provide="slider"
                                   data-slider-ticks="[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"
                                   data-slider-ticks-labels='["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]'
                                   data-slider-min="3"
                                   data-slider-max="10"
                                   data-slider-step="1"
                                   data-slider-value="5"
                                   data-slider-tooltip="hide" />
                        </div>
                        <div>
                            <label for="character_create_agility">Agility</label>
                            <input id="character_create_agility" name="agility" type="text"
                                   data-provide="slider"
                                   data-slider-ticks="[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"
                                   data-slider-ticks-labels='["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]'
                                   data-slider-min="3"
                                   data-slider-max="10"
                                   data-slider-step="1"
                                   data-slider-value="5"
                                   data-slider-tooltip="hide" />
                        </div>
                        <div>
                            <button type="submit" name="submitted" class="btn btn-primary btn-sm" aria-label="Left Align">
                                Save
                            </button>
                            <a class="btn btn-danger btn-sm text-white" aria-label="Left Align" href="{{ route("character.destroy", ["id" => $character->id]) }}">
                                Delete
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php endforeach ?>

        <form method="POST" action="{{ route("character.store") }}">
            <div class="<?php /*form-row align-items-center*/ ?>">
                @csrf
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="character_create_name">Name</span>
                    </div>
                    <input type="text" name="name" class="form-control" id="character_create_name">

                    <input type="checkbox" checked data-toggle="toggle" data-onstyle="outline-success" data-offstyle="outline-danger" value="1" name="status" id="character_create_status">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend ml-3">
                        <label class="input-group-text" for="character_create_race">Race</label>
                    </div>
                    <select class="custom-select" id="character_create_race" name="race">
                        <option value="human">human</option>
                    </select>

                    <div class="input-group-prepend ml-3">
                        <span class="input-group-text" id="character_create_age">Age</span>
                    </div>
                    <input type="text" name="age" class="form-control" id="character_create_age" value="20">

                    <div class="input-group-prepend ml-3">
                        <label class="input-group-text" for="character_create_gender">Gender</label>
                    </div>
                    <select class="custom-select" id="character_create_gender" name="gender">
                        <option value="male">male</option>
                        <option value="female">other</option>
                    </select>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend ml-3">
                        <label class="input-group-text" for="character_create_skin_color">Skin color</label>
                    </div>
                    <select class="custom-select" id="character_create_skin_color" name="skin_color">
                        <option value="white">white</option>
                        <option value="black">black</option>
                    </select>

                    <div class="input-group-prepend ml-3">
                        <label class="input-group-text" for="character_create_eye_color">Eye color</label>
                    </div>
                    <select class="custom-select" id="character_create_eye_color" name="eye_color">
                        <option value="brown">brown</option>
                        <option value="grey">grey</option>
                        <option value="green">green</option>
                        <option value="blue">blue</option>
                        <option value="red">red</option>
                    </select>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend ml-3">
                        <label class="input-group-text" for="character_create_hair_style">Hair style</label>
                    </div>
                    <select class="custom-select" id="character_create_hair_style" name="hair_style">
                        <option value="bold">bold</option>
                        <option value="short">short</option>
                    </select>

                    <div class="input-group-prepend ml-3">
                        <label class="input-group-text" for="character_create_hair_color">Hair color</label>
                    </div>
                    <select class="custom-select" id="character_create_hair_color" name="hair_color">
                        <option value="black">black</option>
                        <option value="white">white</option>
                        <option value="brown">brown</option>
                        <option value="grey">grey</option>
                        <option value="green">green</option>
                        <option value="blue">blue</option>
                        <option value="red">red</option>
                    </select>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend ml-3">
                        <span class="input-group-text" id="character_create_free">Attribute Points</span>
                    </div>
                    <input type="text" name="_attribute_points" disabled class="form-control" id="character_create_free" value="3">
                </div>
                <div>
                    <label for="character_create_strength">Strength</label>
                    <input id="character_create_strength" name="strength" type="text"
                           data-provide="slider"
                           data-slider-ticks="[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"
                           data-slider-ticks-labels='["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]'
                           data-slider-min="3"
                           data-slider-max="6"
                           data-slider-step="1"
                           data-slider-value="5"
                           data-slider-tooltip="hide" />
                </div>
                <div>
                    <label for="character_create_intelligence">Intelligence</label>
                    <input id="character_create_intelligence" name="intelligence" type="text"
                           data-provide="slider"
                           data-slider-ticks="[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"
                           data-slider-ticks-labels='["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]'
                           data-slider-min="3"
                           data-slider-max="10"
                           data-slider-step="1"
                           data-slider-value="5"
                           data-slider-tooltip="hide" />
                </div>
                <div>
                    <label for="character_create_agility">Agility</label>
                    <input id="character_create_agility" name="agility" type="text"
                           data-provide="slider"
                           data-slider-ticks="[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"
                           data-slider-ticks-labels='["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]'
                           data-slider-min="3"
                           data-slider-max="10"
                           data-slider-step="1"
                           data-slider-value="5"
                           data-slider-tooltip="hide" />
                </div>
                <div>
                    <button type="submit" name="submitted" class="btn btn-primary btn-sm" aria-label="Left Align">
                        Save
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
