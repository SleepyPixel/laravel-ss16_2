<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('game/{id}', 'GameController@show')->middleware('auth', 'character');
Route::get('user/character', 'CharacterController@index')->middleware('auth')->name('character.index');
Route::post('user/character/store', 'CharacterController@store')->middleware('auth')->name('character.store');
Route::post('user/character/update/{id}', 'CharacterController@update')->middleware('auth')->name('character.update');
Route::get('user/character/destroy/{id}', 'CharacterController@destroy')->middleware('auth')->name('character.destroy');
Route::resource('user', 'UserController');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
