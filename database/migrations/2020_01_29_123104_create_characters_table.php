<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('name');
            $table->tinyInteger('status')->default(0);
            $table->string('race');
            $table->string('age');
            $table->string('gender');
            $table->string('skin_color');
            $table->string('eye_color');
            $table->string('hair_style');
            $table->string('hair_color');
            $table->tinyInteger('attributes_total');
            $table->unsignedTinyInteger('strength');
            $table->unsignedTinyInteger('intelligence');
            $table->unsignedTinyInteger('agility');
            $table->timestamps();
            $table->softDeletes();

            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character');
    }
}
